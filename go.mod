module github.com/Eldius/message-server-go

go 1.14

require (
	github.com/Eldius/webapp-healthcheck-go v0.0.0-20201007021704-32f7a54f5c99
	github.com/google/uuid v1.1.2
	github.com/jinzhu/gorm v1.9.16
	github.com/mitchellh/go-homedir v1.1.0
	github.com/sirupsen/logrus v1.7.0
	github.com/spf13/cobra v1.0.0
	github.com/spf13/viper v1.7.1
	golang.org/x/crypto v0.0.0-20201002170205-7f63de1d35b0
)
